// LLDI

#include <iostream>
using namespace std;

struct nod{
	int val;
	nod *ls, *ld;
};



void afis(nod *cap){
	for(nod *p=cap;p;p=p->ld){
		cout << p->val << " ";
	}
	cout << endl;
}

void afisInv(nod *cap){
	nod *p;
	for(p=cap;p->ld;p=p->ld);

	for(;p;p=p->ls){
		cout << p->val << " ";
	}
	cout << endl;
}

void creare(nod *&cap){
	cap=NULL;
}

void ad_inc(nod *&cap ,int x){
	if (cap==NULL){
		cap = new nod;
		cap->val = x;
		cap->ls=cap->ld = NULL;
	}
	else{
		nod *p= new nod;
		p->val = x;
		p->ls = NULL;
		p->ld = cap;
		cap->ls = p;
		cap = p;
	}
	
}


int main(){
	nod *cap;

	creare(cap);
	for(int i=1;i<=10;i++){
		ad_inc(cap,i);
	}
	afis(cap);
	afisInv(cap);

	
}